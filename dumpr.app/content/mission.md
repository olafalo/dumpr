+++
title = "Our Mission"
image = "woman-sitting.jpg"
+++

Billions of people use the toilet every day, and of those people, many also have a smartphone. Dumpr® was founded with the goal of finally bringing those people together.

Called "the first meaningful technical advancement in the last 30 years" by [TODO: make up name], Dumpr® allows users to form personal connections like never before. Since all users are required to be on the toilet while using the app, the setting is more intimate than any other social network can ever be.

With Dumpr® Premium, you can take the relationships to the next level. For only $19.99 USD per month, you can see the real-time location, full name, and poop schedule for anyone you meet!

Of course, we also respect our users right to privacy. Every user of Dumpr® always has the right to prevent other users from seeing all of their personal info, by presenting a notarized form in person at our home office in Moscow. Please note that your personal data may still be used by Dumpr® for purposes of conducting business, including, but not limited to, selling targeted ad space, selling as much personal data as we can get our hands on to literally anyone who offers money, and allowing users of our $39.99/mo UltraPremium™ plan to see your personal information anyway.
