+++
title = "Investing"
image = "businessman-coffee.jpg"
+++

Dumpr® has raised $300MM so far in its second round of funding, and is backed directly by Warren Buffet and Ajit Pai, among others. We are currently valued at $6.3B.

However, we are also accepting investments from individuals. We are an app for the people, after all! You can mail your personal contribution in the form of a check or cash to [TODO: get P.O. box for investments]. If we deposit your check, it means your investment was accepted, and you'll be hearing back from us "soon" about how much return you've earned!
